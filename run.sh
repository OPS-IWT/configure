#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt update ; apt upgrade -y

function cron_apt(){
  apt install -y cron-apt
  echo "upgrade -y -o APT::Get::Show-Upgraded=true" > /etc/cron-apt/action.d/4-YesUpgrade
  echo "0 2 * * SUN     root    test -x /usr/sbin/cron-apt && /usr/sbin/cron-apt" >> /etc/cron.d/cron-apt
  sed -i '/^0 4/d' /etc/cron.d/cron-apt
}

function fail_2_ban(){
  apt install -y fail2ban
  fail2ban-client set sshd addignoreip 127.0.0.1
  fail2ban-client set sshd addignoreip 92.154.108.100
  fail2ban-client set sshd addignoreip 163.172.174.162
  fail2ban-client set sshd addignoreip 51.210.100.222
  fail2ban-client set sshd addignoreip 51.210.177.32
}

function main(){
  cron_apt
  #fail_2_ban
}
main
